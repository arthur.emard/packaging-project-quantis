/*
 * Quantis Sàrl CONFIDENTIAL
 * Unpublished Copyright (c) 2009-2017 Quantis SARL, All Rights Reserved.
 * NOTICE: All information contained herein is, and remains the property of Quantis Sàrl. The intellectual and
 * technical concepts contained herein are proprietary to Quantis Sàrl and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material is strictly forbidden unless prior written
 * permission is obtained from Quantis Sàrl. Access to the source code contained herein is hereby forbidden to anyone
 * except current Quantis Sàrl employees, managers or contractors who have executed Confidentiality and Non-disclosure
 * agreements explicitly covering such access.
 * The copyright notice above does not evidence any actual or intended publication or disclosure of this source code,
 * which includes information that is confidential and/or proprietary, and is a trade secret, of Quantis Sàrl.
 * ANY REPRODUCTION, MODIFICATION, DISTRIBUTION, PUBLIC PERFORMANCE, OR PUBLIC DISPLAY OF OR THROUGH USE OF THIS SOURCE
 * CODE WITHOUT THE EXPRESS WRITTEN CONSENT OF Quantis Sàrl IS STRICTLY PROHIBITED, AND IN VIOLATION OF APPLICABLE LAWS
 * AND INTERNATIONAL TREATIES. THE RECEIPT OR POSSESSION OF THIS SOURCE CODE AND/OR RELATED INFORMATION DOES NOT CONVEY
 * OR IMPLY ANY RIGHTS TO REPRODUCE, DISCLOSE OR DISTRIBUTE ITS CONTENTS, OR TO MANUFACTURE, USE, OR SELL ANYTHING THAT
 * IT MAY DESCRIBE, IN WHOLE OR IN PART.
 */
package com.quantis_intl.rfc.ecopack;

import com.google.common.collect.ImmutableMap;
import com.google.inject.Key;
import com.quantis_intl.login.LoginFeature;
import com.quantis_intl.login.shiro.QtsShiroWebModule;
import com.quantis_intl.rfc.ecopack.dao.MybatisRfcUserDao;
import com.quantis_intl.rfc.ecopack.guice.CoreModule;
import com.quantis_intl.stack.QtsStack;
import com.quantis_intl.stack.features.MailFeature;
import com.quantis_intl.stack.features.MyBatisFeature;
import com.quantis_intl.stack.mybatis.MapTypeHandler;
import com.quantis_intl.stack.mybatis.QidTypeHandler;
import org.apache.shiro.guice.web.ShiroWebModule;

import java.util.Properties;

import static com.quantis_intl.stack.utils.StackProperties.*;

public class Bootstrap
{
    private Bootstrap()
    {}

    public static void main(String[] args) throws Exception
    {
        QtsStack stack;
        if (args.length == 1)
            stack = QtsStack.usingPropertiesFile(args[0]);
        else
        {
            stack = QtsStack.newEmptyStack();
            stack.withAdditionalProperties(getDefaultProperties());
        }

        stack.withDefaultObjectMapper()
             .withFeatures(MailFeature.withGmailSender(),
                           new LoginFeature.Builder("q2o9zurVUMrfZaQGZmFt", MybatisRfcUserDao.class)
                                   .withFilter
                                           (ImmutableMap.<String, Key<?>[]>builder().put("/logout", new
                                                   Key<?>[]{QtsShiroWebModule
                                                                    .QTS_LOGOUT})
                                                                                    .put("/priv/**", new Key<?>[]{
                                                                                            QtsShiroWebModule.QTS_PRIV})
                                                                                    .put("/api/pub/**",
                                                                                         new Key<?>[]{
                                                                                                 ShiroWebModule.ANON})
                                                                                    .put("/api/admin/**", new Key<?>[]{
                                                                                            QtsShiroWebModule
                                                                                                    .QTS_AUTHC})
                                                                                    // FIXME: Why is this not working
                                                                                    // anymore?
                                                                            /*,
                                                                                                       QtsShiroWebModule.configFilter
                                                                                                       (QtsShiroWebModule.QTS_ROLES,
                                                                                                       RoleDao.ADMIN)*/
                                                                                    .put("/**", new Key<?>[]{
                                                                                            QtsShiroWebModule
                                                                                                    .QTS_AUTHC})
                                                                                    .build())
                                   .insecure()
                                   .build(),
                           MyBatisFeature.withMapperPackages("com.quantis_intl.login.mappers",
                                                             "com.quantis_intl.rfc.ecopack.mappers")
                                         .withTypeHandler(QidTypeHandler.class)
                                         .withTypeHandler(MapTypeHandler.class))
             .withAdditionalModules(new CoreModule());

        stack.start();
    }

    private static Properties getDefaultProperties()
    {
        Properties p = new Properties();
        p.setProperty(SERVER_PORT_PROP, "7879");
        p.setProperty(SERVER_BASE_CONTEXT_PROP, "/");
        p.setProperty(SERVER_WEB_FOLDER_PROP, "src/main/dart/web");
        // FIXME: Use, might be needed (confidential documents?)
        // p.setProperty("server.appResourcesFolder", "src/main/dart/web");
        p.setProperty(SERVER_LOG_FOLDER_PROP, "");
        p.setProperty(SERVER_DEV_MODE, Boolean.toString(true));
        p.setProperty(ROOT_URL_PROP, "");
        // FIXME: Upload feature might be needed
        //p.setProperty(StackProperties.SERVER_UPLOADED_FILE_FOLDER, Objects.requireNonNull(
        //System.getenv("HELLO_UPLOADED_FILES_FOLDER")));
        p.setProperty(MAIL_APP_PREFIX_PROP, "");
        // FIXME: Do we have a specific mailer?
        p.setProperty(MAIL_FROM_PROP, "");
        p.setProperty(MAILER_USERNAME_PROP, "");
        p.setProperty(MAILER_PASSWORD_PROP, "");
        // FIXME: Which mail for who?
        p.setProperty("forms.mail.to", "");
        p.setProperty(SQL_SCHEMA_PROP, "rfc_ecopack_test");
        p.setProperty(SQL_USERNAME_PROP, "root");
        p.setProperty(SQL_PASSWORD_PROP, "root");

        p.setProperty("shiro.globalSessionTimeout", ((Long) (3L * 60L * 1000L)).toString());
        p.setProperty("shiro.sessionValidationInterval", ((Long) (3L * 60L * 1000L)).toString());

        return p;
    }
}
