/*
 * Quantis Sàrl CONFIDENTIAL
 * Unpublished Copyright (c) 2009-2018 Quantis SARL, All Rights Reserved.
 * NOTICE: All information contained herein is, and remains the property of Quantis Sàrl. The intellectual and
 * technical concepts contained herein are proprietary to Quantis Sàrl and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material is strictly forbidden unless prior written
 * permission is obtained from Quantis Sàrl. Access to the source code contained herein is hereby forbidden to anyone
 * except current Quantis Sàrl employees, managers or contractors who have executed Confidentiality and Non-disclosure
 * agreements explicitly covering such access.
 * The copyright notice above does not evidence any actual or intended publication or disclosure of this source code,
 * which includes information that is confidential and/or proprietary, and is a trade secret, of Quantis Sàrl.
 * ANY REPRODUCTION, MODIFICATION, DISTRIBUTION, PUBLIC PERFORMANCE, OR PUBLIC DISPLAY OF OR THROUGH USE OF THIS SOURCE
 * CODE WITHOUT THE EXPRESS WRITTEN CONSENT OF Quantis Sàrl IS STRICTLY PROHIBITED, AND IN VIOLATION OF APPLICABLE LAWS
 * AND INTERNATIONAL TREATIES. THE RECEIPT OR POSSESSION OF THIS SOURCE CODE AND/OR RELATED INFORMATION DOES NOT CONVEY
 * OR IMPLY ANY RIGHTS TO REPRODUCE, DISCLOSE OR DISTRIBUTE ITS CONTENTS, OR TO MANUFACTURE, USE, OR SELL ANYTHING THAT
 * IT MAY DESCRIBE, IN WHOLE OR IN PART.
 */
package com.quantis_intl.rfc.ecopack.api;


import java.time.LocalDateTime;
import java.util.List;

import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;

import org.apache.shiro.SecurityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.inject.Provider;
import com.quantis_intl.rfc.ecopack.dao.ComparisonProjectDao;
import com.quantis_intl.rfc.ecopack.dao.RfcUserDao;
import com.quantis_intl.rfc.ecopack.model.ComparisonProject;
import com.quantis_intl.rfc.ecopack.model.ComparisonProject.ProjectEditableFields;
import com.quantis_intl.rfc.ecopack.model.Status;
import com.quantis_intl.rfc.ecopack.service.ComparisonProjectService;
import com.quantis_intl.stack.utils.Qid;

@Path("/comparison-projects")
@Produces(MediaType.APPLICATION_JSON)
public class ComparisonProjectApi
{
    private static final Logger LOGGER = LoggerFactory.getLogger(ComparisonProjectApi.class);

    private final ComparisonProjectDao dao;
    private final ComparisonProjectService service;
    private final Provider<RfcUserDao> userDao;

    @Inject
    public ComparisonProjectApi(ComparisonProjectDao dao,
                                ComparisonProjectService service,
                                Provider<RfcUserDao> userDao)
    {
        this.dao = dao;
        this.service = service;
        this.userDao = userDao;
    }

    @GET
    @Path("")
    public List<ComparisonProject> getComparisonProjects()
    {
        List<ComparisonProject> projects;

        if(isAdmin())
            projects = dao.getAllComparisonProjects();
        else
            projects = dao.getAllComparisonProjects(getUserId());

        return projects;
    }

    @POST
    @Path("{idComparisonProject}/open")
    public void openComparisonProject(@PathParam("idComparisonProject") int idComparisonProject)
    {
        ComparisonProject p = service.ensureProjectExistsAndIsOwnedOrSharedWith(getUserId(), idComparisonProject);
        p.open();
        dao.updateLastOpenedDate(p);
        LOGGER.info("Comparison project opened {}", p.getId());
    }

    @POST
    @Path("")
    @Consumes(MediaType.APPLICATION_JSON)
    public ComparisonProject addComparisonProject(ProjectEditableFieldsImpl fields)
    {
        ComparisonProject res = new ComparisonProject(getUserId(),
                                                      fields.getName(),
                                                      Status.DRAFT.ordinal(),
                                                      LocalDateTime.now(),
                                                      fields.getDescription()
        );
        service.addComparisonProject(res);
        LOGGER.info("Added new comparison project {}", res.getId());
        return res;
    }

    @POST
    @Path("{idComparisonProject}/edit-status")
    @Consumes(MediaType.APPLICATION_JSON)
    public void editComparisonProjectStatus(@PathParam("idComparisonProject") int idComparisonProject,
                                            Status status)
    {
        ComparisonProject p = service.ensureProjectExistsAndIsOwnedBy(getUserId(), idComparisonProject);
        p.setStatusId(status.ordinal());
        dao.updateComparisonProjectStatus(p);
        LOGGER.info("Comparison project status updated {}", p.getId());
    }

    @POST
    @Path("{idComparisonProject}")
    @Consumes(MediaType.APPLICATION_JSON)
    public void editComparisonProject(@PathParam("idComparisonProject") int idComparisonProject,
            ProjectEditableFieldsImpl fields)
    {
        ComparisonProject p = service.ensureProjectExistsAndIsOwnedOrSharedWith(getUserId(), idComparisonProject);
        p.updateEditableFields(fields);
        dao.updateComparisonProjectInfoFields(p);
        LOGGER.info("Comparison project updated {}", p.getId());
    }

    @DELETE
    @Path("{idComparisonProject}")
    public void deleteComparisonProject(@PathParam("idComparisonProject") int idComparisonProject)
    {
        service.deleteComparisonProject(getUserId(), idComparisonProject);
        LOGGER.info("Comparison project deleted: {}", idComparisonProject);
    }

    private Qid getQid()
    {
        return ((Qid) SecurityUtils.getSubject().getPrincipal());
    }

    private String getUserId()
    {
        return getQid().getRepresentation();
    }

    private Boolean isAdmin()
    {
        return SecurityUtils.getSubject().hasRole("ADMIN");
    }

    public static class ProjectEditableFieldsImpl implements ProjectEditableFields
    {
        private String name;
        private String description;

        @Override
        public String getName()
        {
            return name;
        }

        @Override
        public String getDescription()
        {
            return description;
        }
    }

}
