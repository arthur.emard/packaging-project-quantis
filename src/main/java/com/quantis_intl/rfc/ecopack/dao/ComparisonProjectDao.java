/*
 * Quantis Sàrl CONFIDENTIAL
 * Unpublished Copyright (c) 2009-2018 Quantis SARL, All Rights Reserved.
 * NOTICE: All information contained herein is, and remains the property of Quantis Sàrl. The intellectual and
 * technical concepts contained herein are proprietary to Quantis Sàrl and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material is strictly forbidden unless prior written
 * permission is obtained from Quantis Sàrl. Access to the source code contained herein is hereby forbidden to anyone
 * except current Quantis Sàrl employees, managers or contractors who have executed Confidentiality and Non-disclosure
 * agreements explicitly covering such access.
 * The copyright notice above does not evidence any actual or intended publication or disclosure of this source code,
 * which includes information that is confidential and/or proprietary, and is a trade secret, of Quantis Sàrl.
 * ANY REPRODUCTION, MODIFICATION, DISTRIBUTION, PUBLIC PERFORMANCE, OR PUBLIC DISPLAY OF OR THROUGH USE OF THIS SOURCE
 * CODE WITHOUT THE EXPRESS WRITTEN CONSENT OF Quantis Sàrl IS STRICTLY PROHIBITED, AND IN VIOLATION OF APPLICABLE LAWS
 * AND INTERNATIONAL TREATIES. THE RECEIPT OR POSSESSION OF THIS SOURCE CODE AND/OR RELATED INFORMATION DOES NOT CONVEY
 * OR IMPLY ANY RIGHTS TO REPRODUCE, DISCLOSE OR DISTRIBUTE ITS CONTENTS, OR TO MANUFACTURE, USE, OR SELL ANYTHING THAT
 * IT MAY DESCRIBE, IN WHOLE OR IN PART.
 */
package com.quantis_intl.rfc.ecopack.dao;

import com.quantis_intl.rfc.ecopack.model.ComparisonProject;
import java.util.List;

public interface ComparisonProjectDao
{
    List<ComparisonProject> getAllComparisonProjects(String userId);

    List<ComparisonProject> getAllComparisonProjects();

    ComparisonProject findExistingProject(int idComparisonProject);

    void addComparisonProject(ComparisonProject res);

    void updateLastOpenedDate(ComparisonProject p);

    void updateComparisonProjectStatus(ComparisonProject p);

    void updateComparisonProjectInfoFields(ComparisonProject p);

    void updateProjectPublic(ComparisonProject p);

    void deleteComparisonProject(int idComparisonProject);

    public static class ComparisonProjectNotFound extends RuntimeException
    {
        private static final long serialVersionUID = -5441677402692883612L;

        public ComparisonProjectNotFound(int idComparisonProject)
        {
            super(Integer.toString(idComparisonProject));
        }
    }
}
