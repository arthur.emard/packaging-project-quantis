/*
 * Quantis Sàrl CONFIDENTIAL
 * Unpublished Copyright (c) 2009-2018 Quantis SARL, All Rights Reserved.
 * NOTICE: All information contained herein is, and remains the property of Quantis Sàrl. The intellectual and
 * technical concepts contained herein are proprietary to Quantis Sàrl and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material is strictly forbidden unless prior written
 * permission is obtained from Quantis Sàrl. Access to the source code contained herein is hereby forbidden to anyone
 * except current Quantis Sàrl employees, managers or contractors who have executed Confidentiality and Non-disclosure
 * agreements explicitly covering such access.
 * The copyright notice above does not evidence any actual or intended publication or disclosure of this source code,
 * which includes information that is confidential and/or proprietary, and is a trade secret, of Quantis Sàrl.
 * ANY REPRODUCTION, MODIFICATION, DISTRIBUTION, PUBLIC PERFORMANCE, OR PUBLIC DISPLAY OF OR THROUGH USE OF THIS SOURCE
 * CODE WITHOUT THE EXPRESS WRITTEN CONSENT OF Quantis Sàrl IS STRICTLY PROHIBITED, AND IN VIOLATION OF APPLICABLE LAWS
 * AND INTERNATIONAL TREATIES. THE RECEIPT OR POSSESSION OF THIS SOURCE CODE AND/OR RELATED INFORMATION DOES NOT CONVEY
 * OR IMPLY ANY RIGHTS TO REPRODUCE, DISCLOSE OR DISTRIBUTE ITS CONTENTS, OR TO MANUFACTURE, USE, OR SELL ANYTHING THAT
 * IT MAY DESCRIBE, IN WHOLE OR IN PART.
 */
package com.quantis_intl.rfc.ecopack.dao;

import java.util.Collection;
import java.util.List;

import javax.inject.Inject;

import org.mybatis.guice.transactional.Transactional;

import com.quantis_intl.rfc.ecopack.mappers.ComparisonProjectMapper;
import com.quantis_intl.rfc.ecopack.model.ComparisonProject;

public class MybatisComparisonProjectDao implements ComparisonProjectDao
{
    private final ComparisonProjectMapper mapper;

    @Inject
    public MybatisComparisonProjectDao(ComparisonProjectMapper mapper)
    {
        this.mapper = mapper;
    }

    @Override
    @Transactional
    public List<ComparisonProject> getAllComparisonProjects(String userId)
    {
        return mapper.getAllComparisonProjectsFromUser(userId);
    }

    @Override
    @Transactional
    public List<ComparisonProject> getAllComparisonProjects()
    {
        return mapper.getAllComparisonProjects();
    }
    @Override
    @Transactional
    public ComparisonProject findExistingProject(int idComparisonProject)
    {
        ComparisonProject res = mapper.getProjectFromId(idComparisonProject);
        if (res == null)
            throw new ComparisonProjectNotFound(idComparisonProject);
        return res;
    }

    @Override
    @Transactional
    public void addComparisonProject(ComparisonProject res)
    {
        mapper.insertComparisonProject(res);
    }

    @Override
    @Transactional
    public void updateLastOpenedDate(ComparisonProject p)
    {
        mapper.updateLastOpenedDate(p);
    }

    @Override
    @Transactional
    public void updateComparisonProjectStatus(ComparisonProject p)
    {
        mapper.updateComparisonProjectStatus(p);
    }

    @Override
    @Transactional
    public void updateComparisonProjectInfoFields(ComparisonProject p)
    {
        mapper.updateComparisonProjectInfoFields(p);
    }

    @Override
    @Transactional
    public void updateProjectPublic(ComparisonProject p)
    {
        mapper.updateProjectPublicField(p);
    }

    @Override
    public void deleteComparisonProject(int idComparisonProject)
    {
        mapper.deleteComparisonProject(idComparisonProject);
    }
}
