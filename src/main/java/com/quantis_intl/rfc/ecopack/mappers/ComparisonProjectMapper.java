/*
 * Quantis Sàrl CONFIDENTIAL
 * Unpublished Copyright (c) 2009-2018 Quantis SARL, All Rights Reserved.
 * NOTICE: All information contained herein is, and remains the property of Quantis Sàrl. The intellectual and
 * technical concepts contained herein are proprietary to Quantis Sàrl and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material is strictly forbidden unless prior written
 * permission is obtained from Quantis Sàrl. Access to the source code contained herein is hereby forbidden to anyone
 * except current Quantis Sàrl employees, managers or contractors who have executed Confidentiality and Non-disclosure
 * agreements explicitly covering such access.
 * The copyright notice above does not evidence any actual or intended publication or disclosure of this source code,
 * which includes information that is confidential and/or proprietary, and is a trade secret, of Quantis Sàrl.
 * ANY REPRODUCTION, MODIFICATION, DISTRIBUTION, PUBLIC PERFORMANCE, OR PUBLIC DISPLAY OF OR THROUGH USE OF THIS SOURCE
 * CODE WITHOUT THE EXPRESS WRITTEN CONSENT OF Quantis Sàrl IS STRICTLY PROHIBITED, AND IN VIOLATION OF APPLICABLE LAWS
 * AND INTERNATIONAL TREATIES. THE RECEIPT OR POSSESSION OF THIS SOURCE CODE AND/OR RELATED INFORMATION DOES NOT CONVEY
 * OR IMPLY ANY RIGHTS TO REPRODUCE, DISCLOSE OR DISTRIBUTE ITS CONTENTS, OR TO MANUFACTURE, USE, OR SELL ANYTHING THAT
 * IT MAY DESCRIBE, IN WHOLE OR IN PART.
 */
package com.quantis_intl.rfc.ecopack.mappers;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.Iterables;
import com.quantis_intl.rfc.ecopack.model.ComparisonProject;
import com.quantis_intl.stack.mybatis.MybatisQueryHelper;
import com.quantis_intl.stack.mybatis.QsSQL;
import org.apache.ibatis.annotations.*;
import org.apache.ibatis.scripting.xmltags.XMLLanguageDriver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Collection;
import java.util.List;

public interface ComparisonProjectMapper
{
    @Select("SELECT p.* FROM projects p WHERE p.ownerId = #{ownerId}")
    List<ComparisonProject> getAllComparisonProjectsFromUser(@Param("ownerId") String ownerId);

    @Select("SELECT p.* FROM projects p")
    List<ComparisonProject> getAllComparisonProjects();

    @SelectProvider(type = ComparisonProjectQueryBuilder.class, method = "selectProjectFromId")
    ComparisonProject getProjectFromId(@Param("id") int idComparisonProject);

    @InsertProvider(type = ComparisonProjectQueryBuilder.class, method = "insertProject")
    @Options(useGeneratedKeys = true, keyProperty = ComparisonProjectQueryBuilder.FIELD_ID)
    void insertComparisonProject(ComparisonProject res);

    @UpdateProvider(type = ComparisonProjectQueryBuilder.class, method = "updateLastOpenedDate")
    void updateLastOpenedDate(ComparisonProject p);

    @UpdateProvider(type = ComparisonProjectQueryBuilder.class, method = "updateProjectStatus")
    void updateComparisonProjectStatus(ComparisonProject p);

    @UpdateProvider(type = ComparisonProjectQueryBuilder.class, method = "updateProjectInfoFields")
    void updateComparisonProjectInfoFields(ComparisonProject p);

    @UpdateProvider(type = ComparisonProjectQueryBuilder.class, method = "updateProjectPublicField")
    void updateProjectPublicField(ComparisonProject p);

    @DeleteProvider(type = ComparisonProjectQueryBuilder.class, method = "deleteProject")
    void deleteComparisonProject(@Param(ComparisonProjectQueryBuilder.FIELD_ID) int idComparisonProject);


    class ComparisonProjectQueryBuilder
    {
        static final String TABLE_NAME = "projects";
        static final String FIELD_ID = "id";
        static final String FIELD_OWNER_ID = "ownerId";
        static final String FIELD_LAST_OPENED_DATE = "lastOpenedDate";
        static final String FIELD_NAME = "name";
        static final String FIELD_STATUS = "statusId";
        static final String FIELD_CREATION_DATE = "creationDate";
        static final String FIELD_IS_PUBLIC = "isPublic";

        static final ImmutableList<String> ALL_EXCEPT_ID_FIELDS = ImmutableList.of(
                FIELD_OWNER_ID, FIELD_LAST_OPENED_DATE, FIELD_NAME, FIELD_STATUS,
                FIELD_CREATION_DATE, FIELD_IS_PUBLIC);

        static final ImmutableList<String> ALL_FIELDS = ImmutableList
                .copyOf(Iterables.concat(ImmutableList.of(FIELD_ID), ALL_EXCEPT_ID_FIELDS));


        private static final Logger LOGGER = LoggerFactory.getLogger(ComparisonProjectQueryBuilder.class);


        public String selectProjectOfUser()
        {
            return new QsSQL()
                    .SELECT(ALL_FIELDS)
                    .FROM(TABLE_NAME)
                    .WHERE_PARAM(FIELD_OWNER_ID)
                    .toString();
        }


        public String selectProjectFromId()
        {
            String res = new QsSQL()
                    .SELECT(ALL_FIELDS)
                    .FROM(TABLE_NAME)
                    .WHERE_PARAM(FIELD_ID)
                    .toString();
            return res;
        }

        public String insertProject()
        {
            return new QsSQL().INSERT_INTO(TABLE_NAME)
                    .VALUES_PARAMS(ALL_FIELDS)
                    .toString();
        }

        public String updateLastOpenedDate()
        {
            return new QsSQL()
                    .UPDATE(TABLE_NAME)
                    .SET_PARAM(FIELD_LAST_OPENED_DATE)
                    .WHERE_PARAM(FIELD_ID)
                    .toString();
        }

        public String updateProjectStatus()
        {
            String res = new QsSQL()
                    .UPDATE(TABLE_NAME)
                    .SET_PARAMS(FIELD_STATUS)
                    .WHERE_PARAM(FIELD_ID)
                    .toString();
            return res;
        }

        public String updateProjectInfoFields()
        {
            String res = new QsSQL()
                    .UPDATE(TABLE_NAME)
                    .SET_PARAMS(FIELD_NAME)
                    .WHERE_PARAM(FIELD_ID)
                    .toString();
            return res;
        }

        public String updateProjectPublicField()
        {
            String res = new QsSQL()
                    .UPDATE(TABLE_NAME)
                    .SET_PARAM(FIELD_IS_PUBLIC)
                    .WHERE_PARAM(FIELD_ID)
                    .toString();
            return res;
        }

        public String deleteProject()
        {
            return new QsSQL()
                    .DELETE_FROM(TABLE_NAME)
                    .WHERE_PARAM(FIELD_ID)
                    .toString();
        }
    }

}
