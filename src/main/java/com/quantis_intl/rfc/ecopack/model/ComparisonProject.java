/*
 * Quantis Sàrl CONFIDENTIAL
 * Unpublished Copyright (c) 2009-2018 Quantis SARL, All Rights Reserved.
 * NOTICE: All information contained herein is, and remains the property of Quantis Sàrl. The intellectual and
 * technical concepts contained herein are proprietary to Quantis Sàrl and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material is strictly forbidden unless prior written
 * permission is obtained from Quantis Sàrl. Access to the source code contained herein is hereby forbidden to anyone
 * except current Quantis Sàrl employees, managers or contractors who have executed Confidentiality and Non-disclosure
 * agreements explicitly covering such access.
 * The copyright notice above does not evidence any actual or intended publication or disclosure of this source code,
 * which includes information that is confidential and/or proprietary, and is a trade secret, of Quantis Sàrl.
 * ANY REPRODUCTION, MODIFICATION, DISTRIBUTION, PUBLIC PERFORMANCE, OR PUBLIC DISPLAY OF OR THROUGH USE OF THIS SOURCE
 * CODE WITHOUT THE EXPRESS WRITTEN CONSENT OF Quantis Sàrl IS STRICTLY PROHIBITED, AND IN VIOLATION OF APPLICABLE LAWS
 * AND INTERNATIONAL TREATIES. THE RECEIPT OR POSSESSION OF THIS SOURCE CODE AND/OR RELATED INFORMATION DOES NOT CONVEY
 * OR IMPLY ANY RIGHTS TO REPRODUCE, DISCLOSE OR DISTRIBUTE ITS CONTENTS, OR TO MANUFACTURE, USE, OR SELL ANYTHING THAT
 * IT MAY DESCRIBE, IN WHOLE OR IN PART.
 */
package com.quantis_intl.rfc.ecopack.model;

import com.google.common.base.Strings;

import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class ComparisonProject
{
    public static final int MAX_NAME_LENGTH = 50;
    public static final int MAX_CONTEXT_LENGTH = 64000;
    public static final int MAX_PROJECT_TEAM_LENGTH = 255;

    int id;
    private String ownerId;
    private LocalDateTime lastOpenedDate;
    private String name;
    private Integer statusId;
    private LocalDateTime creationDate;
    private String description;

    private List<String> sharedWith = new ArrayList<>();
    private boolean isPublic = false;
    private String lockedBy;

    ComparisonProject()
    {}

    public ComparisonProject(String ownerId, String name, Integer statusId, LocalDateTime creationDate, String description)
    {
        this.ownerId = Objects.requireNonNull(ownerId);
        this.setName(name);
        this.open();
        this.setStatusId(statusId);
        this.setCreationDate(creationDate);
        this.setDescription(description);
    }

    public int getId()
    {
        return id;
    }

    public String getOwnerId()
    {
        return ownerId;
    }

    public void setOwnerId(String id)
    {
        this.ownerId = id;
    }

    public LocalDateTime getLastOpenedDate()
    {
        return lastOpenedDate;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        if (Strings.isNullOrEmpty(name) || name.length() > MAX_NAME_LENGTH)
            throw new InvalidProjectName(name);
        this.name = name;
    }

    public List<String> getSharedWith()
    {
        return sharedWith;
    }

    public void setSharedWith(List<String> sharedWith)
    {
        this.sharedWith = sharedWith;
    }

    public boolean getIsPublic()
    {
        return isPublic;
    }

    public void setIsPublic(boolean isPublic)
    {
        this.isPublic = isPublic;
    }

    public Integer getStatusId() {
        return statusId;
    }

    public void setStatusId(Integer statusId) {
        this.statusId = statusId;
    }

    public LocalDateTime getCreationDate()
    {
        return creationDate;
    }

    public void setCreationDate(LocalDateTime creationDate)
    {
        this.creationDate = creationDate;
    }

    public String getDescription()
    {
        return description;
    }

    public void setDescription(String description)
    {
        this.description = description;
    }

    public String getLockedBy()
    {
        return lockedBy;
    }

    public void setLockedBy(String lockedBy)
    {
        this.lockedBy = lockedBy;
    }

    public void updateEditableFields(ProjectEditableFields fields)
    {
        setName(fields.getName());
        setDescription(fields.getDescription());
    }

    public void open()
    {
        this.lastOpenedDate = LocalDateTime.now(ZoneOffset.UTC);
    }


    public static class InvalidProjectName extends RuntimeException
    {
        private static final long serialVersionUID = -691456286328374857L;

        public InvalidProjectName(String name)
        {
            super(name);
        }
    }

    public static interface ProjectEditableFields
    {
        String getName();

        String getDescription();
    }
}
