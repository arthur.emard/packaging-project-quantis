/*
 * Quantis Sàrl CONFIDENTIAL
 * Unpublished Copyright (c) 2009-2018 Quantis SARL, All Rights Reserved.
 * NOTICE: All information contained herein is, and remains the property of Quantis Sàrl. The intellectual and
 * technical concepts contained herein are proprietary to Quantis Sàrl and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material is strictly forbidden unless prior written
 * permission is obtained from Quantis Sàrl. Access to the source code contained herein is hereby forbidden to anyone
 * except current Quantis Sàrl employees, managers or contractors who have executed Confidentiality and Non-disclosure
 * agreements explicitly covering such access.
 * The copyright notice above does not evidence any actual or intended publication or disclosure of this source code,
 * which includes information that is confidential and/or proprietary, and is a trade secret, of Quantis Sàrl.
 * ANY REPRODUCTION, MODIFICATION, DISTRIBUTION, PUBLIC PERFORMANCE, OR PUBLIC DISPLAY OF OR THROUGH USE OF THIS SOURCE
 * CODE WITHOUT THE EXPRESS WRITTEN CONSENT OF Quantis Sàrl IS STRICTLY PROHIBITED, AND IN VIOLATION OF APPLICABLE LAWS
 * AND INTERNATIONAL TREATIES. THE RECEIPT OR POSSESSION OF THIS SOURCE CODE AND/OR RELATED INFORMATION DOES NOT CONVEY
 * OR IMPLY ANY RIGHTS TO REPRODUCE, DISCLOSE OR DISTRIBUTE ITS CONTENTS, OR TO MANUFACTURE, USE, OR SELL ANYTHING THAT
 * IT MAY DESCRIBE, IN WHOLE OR IN PART.
 */
package com.quantis_intl.rfc.ecopack.service;

import org.apache.shiro.SecurityUtils;

import com.google.inject.Inject;
import com.quantis_intl.rfc.ecopack.dao.ComparisonProjectDao;
import com.quantis_intl.rfc.ecopack.model.ComparisonProject;

public class ComparisonProjectService
{
    private final ComparisonProjectDao comparisonProjectDao;

    private static String BASELINE_SCENARIO_NAME = "Baseline";

    @Inject
    public ComparisonProjectService(ComparisonProjectDao comparisonProjectDao)
    {
        this.comparisonProjectDao = comparisonProjectDao;
    }

    public ComparisonProject addComparisonProject(ComparisonProject project)
    {
        comparisonProjectDao.addComparisonProject(project);
        return project;
    }

    public ComparisonProject addComparisonProjectFromLibrary(int idLibrary, ComparisonProject project)
    {
        comparisonProjectDao.addComparisonProject(project);
        return project;
    }

    public void deleteComparisonProject(String ownerId, int idComparisonProject)
    {
        ComparisonProject p = ensureProjectExistsAndIsOwnedBy(ownerId, idComparisonProject);
        comparisonProjectDao.deleteComparisonProject(idComparisonProject);
    }

    public ComparisonProject ensureProjectExistsAndIsOwnedBy(String ownerId, int idComparisonProject)
    {
        ComparisonProject p = comparisonProjectDao.findExistingProject(idComparisonProject);
        ensureProjectIsOwnedBy(ownerId, p);
        return p;
    }

    public void ensureProjectIsOwnedBy(String ownerId, ComparisonProject p)
    {
        if (!p.getOwnerId().equals(ownerId))
            throw new ComparisonProjectDao.ComparisonProjectNotFound(p.getId());
    }

    public ComparisonProject ensureProjectExistsAndIsOwnedOrSharedWith(String ownerId, int idComparisonProject)
    {
        ComparisonProject p = comparisonProjectDao.findExistingProject(idComparisonProject);
        ensureProjectIsOwnedOrSharedWith(ownerId, p);
        return p;
    }

    public void ensureProjectIsOwnedOrSharedWith(String userId, ComparisonProject p)
    {
        if (!isAdmin() && !p.getOwnerId().equals(userId) && p.getSharedWith().stream().noneMatch(userId::equals))
            throw new ComparisonProjectDao.ComparisonProjectNotFound(p.getId());
    }

    private boolean isAdmin()
    {
        return SecurityUtils.getSubject().hasRole("ADMIN");
    }
}
